# -*- coding: utf-8 -*-
"""
Created on Mon Dec  2 21:07:16 2019

@author: da.salazarb
"""
import os
import os.path
import numpy as np
import pandas as pd
from func_JNMF_oneStep import *
from collections import defaultdict

#%% Funcion MMJNMF
def mmjnmf(merged,param):
    """
    Returns W and H.

    Returns
    -------
    W (array)
    H (dict)

    """
#%% Load data and parameters
    np.random.seed(0)
    
    # --------------------------------------------- #
    #### ........... Load parameters ........... ####
    # --------------------------------------------- #
    K=param['K']; r1=param['r1']; L1=param['L1']; L2=param['L2']; r2=param['r2']; d1=param['d1'];
    tol=param['tol']; maxiter=param['maxiter']; stop_control = np.zeros([1,maxiter+1]); stop_control[0,0] =1;
    delta_control = np.zeros([1,maxiter+1]); delta_control[0,0] =1; repeat=param['repeat']; sample=param['sample']; 
    path = merged.path+"/pathFeatureLabel/"; pac_por_comod=param['pac_por_comod']; pac_por_comod_ccle = param['pac_por_comod_ccle']
    
    ## *** Real-world data ***
    ## Verify if there are 2 projects
    project_profile = os.listdir(merged.path+"INPUT DATA");
    
    
    project1 = sorted(list(set([p[0:p.find("_")] for p in project_profile])))[0]; project2 = sorted(list(set([p[0:p.find("_")] for p in project_profile])))[1];
    profiles_project1 = list(set([p.replace(".csv", "")[p.replace(".csv", "").find("_")+1:] for p in project_profile if project1 in p]));
    profiles_project2 = list(set([p.replace(".csv", "")[p.replace(".csv", "").find("_")+1:] for p in project_profile if project2 in p]));
    
    if len(profiles_project1) > len(profiles_project2):
        ccle_tcga = "high"
    elif len(profiles_project1) < len(profiles_project2):
        ccle_tcga = "less"
    else:
        ccle_tcga = "equal"
    
    if ccle_tcga == "high":
        projects = [sorted(list(set([p[0:p.find("_")] for p in project_profile])))[0], sorted(list(set([p[0:p.find("_")] for p in project_profile])))[1]];
    elif ccle_tcga == "less":
        projects = [sorted(list(set([p[0:p.find("_")] for p in project_profile])))[1], sorted(list(set([p[0:p.find("_")] for p in project_profile])))[0]];
    elif ccle_tcga == "equal":
        projects = sorted(list(set([p[0:p.find("_")] for p in project_profile])));
        
    print("    * The projects are {} and {} ".format(projects[1], projects[0]))
    profiles_project_TCGA = list(set([p.replace(".csv", "")[p.replace(".csv", "").find("_")+1:] for p in project_profile if projects[1] in p]));
    profiles_project_CCLE = list(set([p.replace(".csv", "")[p.replace(".csv", "").find("_")+1:] for p in project_profile if projects[0] in p]));
    print("    ** For {}, the profiles are {}".format(projects[1], profiles_project_TCGA))
    print("    ** For {}, the profiles are {}".format(projects[0], profiles_project_CCLE))
    
    if len(profiles_project_CCLE) > len(profiles_project_TCGA):
        miss_profile = list(set(profiles_project_CCLE) - set(profiles_project_TCGA))
        newNamesProfiles = {item[0]: str(i)+"_"+item[0] for i, item in enumerate(merged.perfiles_CCLE.items())};
        featureLabel = {v: merged.perfiles_CCLE[k].infoFeatures["columns"] for k, v in newNamesProfiles.items()};
        predictProject = projects[1]
    elif len(profiles_project_TCGA) > len(profiles_project_CCLE):
        miss_profile = list(set(profiles_project_TCGA) - set(profiles_project_CCLE))
        newNamesProfiles = {item[0]: str(i)+"_"+item[0] for i, item in enumerate(merged.perfiles_TCGA.items())};
        featureLabel = {v: merged.perfiles_TCGA[k].infoFeatures["columns"] for k, v in newNamesProfiles.items()};
        predictProject = projects[0]
    else:
        miss_profile = ["None"]
        newNamesProfiles = {item[0]: str(i)+"_"+item[0] for i, item in enumerate(merged.perfiles_TCGA.items())};
        featureLabel = {v: merged.perfiles_TCGA[k].infoFeatures["columns"] for k, v in newNamesProfiles.items()};
        predictProject = ""
        
    ccle = {newNamesProfiles[k]: v.profile for k,v in merged.perfiles_CCLE.items() if any(k in s for s in profiles_project_CCLE)};
    tcga = {newNamesProfiles[k]: v.profile for k,v in merged.perfiles_TCGA.items() if any(k in s for s in profiles_project_TCGA)};
    
    if len(profiles_project_CCLE) > len(profiles_project_TCGA):
        profile2use = ccle
    elif len(profiles_project_TCGA) > len(profiles_project_CCLE):
        profile2use = tcga
    else:
        profile2use = tcga
    
    theta_record = {newNamesProfiles[key]: value for key, value in merged.constraints_theta.items()};
    R_record = merged.constraints_r;
    
    drug_patients_tcga = merged.perfiles_TCGA[list(merged.perfiles_TCGA.keys())[0]].infoFeatures["rows"]
    mrna_rows_tcga = merged.perfiles_TCGA[list(merged.perfiles_TCGA.keys())[0]].infoFeatures["rows"]
    mrna_rows_ccle = merged.perfiles_CCLE[list(merged.perfiles_TCGA.keys())[0]].infoFeatures["rows"]

    for perfil in newNamesProfiles.values():
        try:
            # Create target Directory
            os.mkdir(path+'/'+perfil)
            print("    * Directory " , path+'/'+perfil ,  " Created ")
        except FileExistsError:
            print("    * Directory " , path+'/'+perfil ,  " already exists")
    
    print("\n    ________________________________________________________________________")
    print("    Running MjNMF : \n")
    print("     1. Hyperparameters: ")
    print("           * K: {} - r1: {} - r2: {} - d1: {} - L1: {} - L2: {}\n".format(K, r1, r2, d1, L1, L2))
    print("     2. Some parameters: ")
    print("           * tol: {} - maxiter: {} - repeat: {} \n".format(tol, maxiter, repeat))
    
    ## Consensus Matrix
    consensus_matrix = defaultdict(lambda: defaultdict(np.ndarray(0)))
    
    for perfil, value in profile2use.items():
        if value.shape[1] > 1000:
            print("")
            print("    * For the profile {}, the coephinetic coefficient will not be calculated since the dimensions are very large. It would take too much time to calculate it.".format(perfil))
            print("")
        else:
            consensus_matrix[perfil] = np.zeros([value.shape[1], value.shape[1]])

# %% The M&M-jNMF method
    # --------------------------------------------- #
    #### ........... M&M-jNMF method ........... ####
    # --------------------------------------------- #
    for index in range(0,repeat):
        
    # ---------------------------------------------- #
    #### ........... Init H_I and W's ........... ####
    # ---------------------------------------------- #
        ## Winit (samples x rank)
        Winit = defaultdict(lambda: np.ndarray(0));
        Winit['W0_ccle'] = np.random.uniform(0,1,[ccle[list(ccle.keys())[0]].shape[0],K]);
        Winit['W0_tcga'] = np.random.uniform(0,1,[tcga[list(tcga.keys())[0]].shape[0],K]);
        
        ## H_record_init (profile x rank x biomolecule)
        H_record_init = defaultdict(lambda: np.ndarray(0));
        H_record_init = {k: np.random.uniform(0,1,[K, v.shape[1]]) for k, v in profile2use.items()};
        
        ## normalization of H and W
        W, H = normalize_WH_rowH(Winit, H_record_init,1);
            
# %% Error computation with initial parameters ('MUR')
        # ------------------------------------------------------- #
        #### ........... Initial error calculation ........... ####
        # ------------------------------------------------------- #
        WtW = defaultdict(dict);
        WtW = {k: np.dot(v.T, v) for k,v in W.items()};
        sumXHt_ccle,sumXHt_tcga,sumHHt,_,_,HHt_record = compute_XHt_HHt(ccle, tcga, H, K);
        Hsumtheta_record, sumHRt_record = compute_HsumTheta_sumHRt_record(H, theta_record, R_record);
        V_ccle = np.concatenate([v for v in ccle.values()],axis=1);
        V_tcga = np.concatenate([v for v in tcga.values()],axis=1);
        VtV_ccle = np.dot(V_ccle.T,V_ccle);
        VtV_tcga = np.dot(V_tcga.T,V_tcga);
        diff2,diff3,diff4_ccle,diff4_tcga,diff5 = compute_diff(WtW,H,HHt_record,Hsumtheta_record,sumHRt_record,L1,L2,r1,r2,d1,K);
        delta1_init,_,_ = StopCriterion_rule1(VtV_ccle,VtV_tcga,WtW,W,sumXHt_ccle,sumHHt,sumXHt_tcga,diff2,diff3,diff4_ccle,diff4_tcga,diff5);
        
#%% MJNMF algorithm
        # -------------------------------------------------------------- #
        #### ........... Multiplicative Update Rule (MUR) ........... ####
        # -------------------------------------------------------------- #
        iteracion = 0;
        W0 = defaultdict(lambda: np.ndarray(0));
        WtX_record_ccle = defaultdict(lambda: np.ndarray(0));
        WtX_record_tcga = defaultdict(lambda: np.ndarray(0));
        WtW = defaultdict(dict);
        # print('* Running the Multiplicative Update Rule')
        while stop_control[0, iteracion] > tol and iteracion < maxiter:

            ## *** W_ccle and Wtcga update by setting Hi
            #% update W_ccle with any Hi fixed
            sumWHHt_ccle = np.dot(W['W0_ccle'],sumHHt["HHt_ccle"]);
            W0['W0_ccle'] = np.divide((W['W0_ccle']*sumXHt_ccle),(sumWHHt_ccle+(r1*W['W0_ccle'])+np.finfo(float).eps));
            
            #% update W_tcga with any Hi fixed
            sumWHHt_tcga = np.dot(W['W0_tcga'],sumHHt["HHt_tcga"]);
            W0['W0_tcga'] = np.divide((W['W0_tcga']*sumXHt_tcga),(sumWHHt_tcga+(r2*W['W0_tcga'])+np.finfo(float).eps));
            
            WtW = {k: np.dot(v.T, v) for k,v in W0.items()};
            
            #% update Hi with W fixed
            WtX_record_ccle = {k: np.dot(W0['W0_ccle'].T,v) for k, v in ccle.items()};
            WtX_record_tcga = {k: np.dot(W0['W0_tcga'].T,v) for k, v in tcga.items()};
            Hsumtheta_record, sumHRt_record = compute_HsumTheta_sumHRt_record(H, theta_record, R_record);
            H0 = compute_actualize_H_record(H, WtX_record_ccle, WtX_record_tcga, Hsumtheta_record, sumHRt_record, WtW, L1, L2, d1, K, miss_profile);
            
            #% normalization
            W, H = normalize_WH_rowH(W0, H0,1);
            
            #% stop criterion
            WtW = {k: np.dot(v.T, v) for k,v in W.items()};
            Hsumtheta_record, sumHRt_record = compute_HsumTheta_sumHRt_record(H, theta_record, R_record);
            sumXHt_ccle,sumXHt_tcga,sumHHt,_,_,HHt_record = compute_XHt_HHt(ccle, tcga, H, K);
            
            diff2,diff3,diff4_ccle,diff4_tcga,diff5 = compute_diff(WtW,H,HHt_record,Hsumtheta_record,sumHRt_record,L1,L2,r1,r2,d1,K);
            delta,diff1_ccle, diff1_tcga = StopCriterion_rule1(VtV_ccle,VtV_tcga,WtW,W,sumXHt_ccle,sumHHt,sumXHt_tcga,diff2,diff3,diff4_ccle,diff4_tcga,diff5);
            if iteracion == 0:
                delta_old = delta1_init;
            iteracion += 1;
            delta_control[0, iteracion] = np.around(delta, decimals=5);
            stop_control[0, iteracion] = np.around(np.abs((delta_old - delta)/(delta1_init - delta)), decimals=12);
            delta_old  = delta;
            if sample != "sample":
                if iteracion % 10 == 0:
                    print('    * We are in iteration {: d} - stop_control: {}'.format(iteracion, stop_control[0, iteracion]))
            else:
                if iteracion % 100 == 0:
                    print('    * We are in iteration {: d} - stop_control: {}'.format(iteracion, stop_control[0, iteracion]))
                    
        print('')
        print('    * Repeat {:d}. Optimum value found in iteration: {:d} -> stop_control: {}'.format(index+1, iteracion, stop_control[0, iteracion]))        
        print('        -> diff1_{}: {} - diff1_{}: {} - diff2: {} - diff3: {}'.format(projects[0], np.round(diff1_ccle,4), projects[1], np.round(diff1_tcga,4), np.round(diff2,4), np.round(diff3,4)))
        print('        -> diff4_{}: {} - diff4_{}: {} - diff5: {}'.format(projects[0], np.round(diff4_ccle,4), projects[1], np.round(diff4_tcga,4), np.round(diff5, 4)))
        print('        -> Objective Function value: {} \n'.format(delta))
        
        try:
            # if delta_control[0][len(np.where(delta_control > 1)[1])-1] < best_delta_control[0][len(np.where(best_delta_control > 1)[1])-1]:
            if stop_control[0][len(np.where(stop_control > 0)[1])-1] < best_stop_control[0][len(np.where(best_stop_control > 0)[1])-1]:
                best_W = W.copy();
                best_H = H.copy();
                best_delta_control = np.copy(delta_control);
                best_stop_control = np.copy(stop_control);
                best_repeat = repeat;
            else:
                pass
        except:
            best_W = W.copy();
            best_H = H.copy();
            best_delta_control = np.copy(delta_control);
            best_stop_control = np.copy(stop_control);
            best_repeat = repeat;
                
        ## Get co-clusters?
        comodule, _, connectivity_matrix = jNMF_module(H,1.5, path, featureLabel, False, sample, "useMalaCard", "H_first_second_max");
        
        for key, val in connectivity_matrix[perfil].items():
            consensus_matrix[perfil][key] += val

# %% Cophenetic coefficient calculation
    # ------------------------------------------------------------------------- #
    #### ........... Consensus matrix and cophenetic coefficient ........... ####
    # ------------------------------------------------------------------------- #
    for perfil in consensus_matrix.keys():
        consensus_matrix[perfil] = (consensus_matrix[perfil] / (repeat))
    
    rho_C = defaultdict(int)
    for perfil in consensus_matrix.keys():
        rho_C[perfil] = sum(sum(4*(consensus_matrix[perfil]-(1/2))**2)) / (consensus_matrix[perfil].shape[1] * consensus_matrix[perfil].shape[1])
        
    for perfil in profile2use.keys():
        if not perfil in list(rho_C.keys()):
            rho_C[perfil] = "NA"

# %% Cluster patients - cell lines old method
    # --------------------------------------------------------------- #
    #### ........... Cluster patients - cell lines ........... ####
    # --------------------------------------------------------------- #
    # /co-mod_R_plots/01_Cluster_Patient-Cell
    module_patient_cell,porc_modulos_no_vacios_ccle_ = jNMF_module_patients_cellLines(best_W, mrna_rows_tcga, mrna_rows_ccle, param, path, sample)
    
    rec_surv = pd.DataFrame(data=np.zeros([best_W["W0_tcga"].shape[0], best_W["W0_tcga"].shape[1]]), index=list(map(lambda x: x, drug_patients_tcga)))
    for i in range(0,best_W["W0_tcga"].shape[0]):
        rec_surv.iloc[i,np.argsort(-best_W["W0_tcga"][i,:], axis=0)[0]] = 1 ## Selecciona la fila de paciente y busca donde es el maximo
        
    def find_nearest(array, value):
        array = np.asarray(array)
        idx = (np.abs(array - value)).argmin()
        return idx
    
    cuantos_co_md = pac_por_comod ## number of patients allowed in each cluster, typically 30
    W_max = np.max(best_W["W0_tcga"], axis=0) # maxima for K clusters
    W_max[np.where(np.sum(rec_surv)<cuantos_co_md)[0]] = -9999 ## which of them are less than how many_co_md
    b = merged.perfiles_TCGA[list(merged.perfiles_TCGA.keys())[0]].infoFeatures["rows"]
    menores_Xpac = np.where(np.sum(rec_surv)<cuantos_co_md)[0]
    for i in range(0,menores_Xpac.shape[0]): ## go through all co-clusters that have less than X patients
        a = rec_surv[menores_Xpac[i]][np.where(rec_surv[menores_Xpac[i]]>0)[0]].index
        if len(a) > 0:
            for j in range(0,len(a)):
                d = np.where(rec_surv.loc[a[j]]>0)[0][0] ## search current position of a[j]
                rec_surv.loc[a[j], d] = 0 ## remove that position
                c = np.max(best_W["W0_tcga"][b.index(a[j]),]) ## maximum in best_W["W0_tcga"] for that row
                rec_surv.loc[a[j],find_nearest(W_max, c)] = 1 ## new position in co-md
                
    ## to finally know which clusters were left
    rec_surv_prepro = pd.melt(rec_surv.reset_index(), id_vars="index")
    rec_surv_prepro  = rec_surv_prepro.loc[rec_surv_prepro.value != 0,:]
    
    rec_surv_prepro.rename(columns={'variable': 'cluster'}, inplace=True)
    rec_surv_prepro.drop(["value"], axis=1, inplace=True)
    rec_surv_prepro.to_csv(path+'/co-mod_observations_clusters/observations_clusters_for_project_'+projects[1]+'_using_best_W.csv')
    
    func_ccle_groups(best_W, mrna_rows_ccle, pac_por_comod_ccle, path, projects)
    
# %% Co-cluster counting by two methods
    # --------------------------------------------------------------------- #
    #### ........... Co-cluster counting: H_first_second_max ........... ####
    # --------------------------------------------------------------------- #
    print_savePath=True
    # comodule, comodule_count, _ = jNMF_module(best_H, 1.5, path, featureLabel, print_savePath, sample, useMalaCard, "weight");
    comodule, comodule_count, _ = jNMF_module(best_H, 1.5, path, featureLabel, print_savePath, sample, "useMalaCard", "H_first_second_max");
    
# %% Prediction of X_{missing}        
    # ------------------------------------------------------------------------------------ #
    #### ........... Drug sensitivity profile calculation for TCGA patients ........... ####
    # ------------------------------------------------------------------------------------ #
    # Supp_File_S9
    if len(miss_profile) > 0 and miss_profile[0] != "None":
        drug_columns_tcga = merged.perfiles_TCGA[miss_profile[0]].infoFeatures["columns"]
        df_drug = np.dot(best_W["W0_tcga"], best_H[newNamesProfiles[miss_profile[0]]]);
        df_drug = pd.DataFrame(data=df_drug, index=drug_patients_tcga, columns=drug_columns_tcga);
        if sample != "sample":
            print("")
            print('----------------------------------------------------------') 
            print("    * Predicting the {} profile for {} project. Saved in /pathFeatureLabel/co-mod_predicted_data".format(miss_profile[0], predictProject))
            merged.perfiles_TCGA[miss_profile[0]].profile = df_drug
            df_drug.to_csv(path+"/co-mod_predicted_data/"+predictProject+"_"+miss_profile[0]+"_predicted.csv") ## guarda el archivo listo para usar de TCGA
            pass
        tcga[newNamesProfiles[miss_profile[0]]] = df_drug.values

# %% Save the best H_I and W's low-rank matrices
    # ----------------------------------------------------- #
    #### ........... Save best_H and best_W ........... ####
    # ----------------------------------------------------- #
    folder=path+"/co-mod_bestW_and_bestH/"
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        os.unlink(file_path)
        
    for k, v in best_W.items():
        if k == "W0_ccle":
            k = projects[0]
        else:
            k = projects[1]
            
        np.savetxt(path+"/co-mod_bestW_and_bestH/best_W_"+k+'_'+str(K)+'-r1_'+str(r1)+'-r2_'+str(r2)+'-L1_'+str(L1)+'-L2_'+str(L2)+'-d1_'+str(d1)+'.csv', v, delimiter=',')
    for k, v in best_H.items():
        np.savetxt(path+"/co-mod_bestW_and_bestH/best_H_"+k+'_'+str(K)+'-r1_'+str(r1)+'-r2_'+str(r2)+'-L1_'+str(L1)+'-L2_'+str(L2)+'-d1_'+str(d1)+'.csv', v, delimiter=',')
    print('----------------------------------------------------------') 
    print("")
    print("")
    
# %% Outputs
    # ------------------------------------- #
    #### ........... Outputs ........... ####
    # ------------------------------------- #
    paramResults = {'ccle': ccle, 'tcga': tcga, 'best_W': best_W, 'best_H': best_H, 'comodule': comodule, 'module_patient_cell': module_patient_cell,
                    'rho_C': rho_C, 'delta_control': delta_control, 'best_stop_control': best_stop_control, 'FO': delta};
    
    return paramResults