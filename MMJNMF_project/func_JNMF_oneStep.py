# -*- coding: utf-8 -*-
"""
Created on Mon Dec  2 21:07:43 2019

@author: da.salazarb
"""

#%% Funciones JNMF.py
# import re
import os
import subprocess
import numpy as np
import pandas as pd
from collections import defaultdict
from itertools import combinations, permutations, product
def roc_curve_MjNMF(y_true, y_pred):
    ## https://stackoverflow.com/questions/61321778/how-to-calculate-tpr-and-fpr-in-python-without-using-sklearn
    # from sklearn.metrics import confusion_matrix
    # cnf_matrix = confusion_matrix(y_true, y_pred)
    FP = np.sum((y_pred == 1) & (y_true == 0))
    FN = np.sum((y_pred == 0) & (y_true == 1))
    TP = np.sum((y_pred == 1) & (y_true == 1))
    TN = np.sum((y_pred == 0) & (y_true == 0))
    
    # Sensitivity, hit rate, recall, or true positive rate
    TPR = TP/(TP+FN)
    FPR = FP/(FP+TN)
    
    return TPR, FPR

def new_syntheticData(dim_features, dim_samples, numero_grupos_W, numero_grupos_H, ruta, K):
    ## descomentar para generar nuevos datos!
    ## W
    add_W_ccle = np.zeros([dim_samples[0], K])
    add_W_tcga = np.zeros([dim_samples[1], K])
    groups_W  = np.random.randint(numero_grupos_W, size=dim_samples[0]); ## se asignan las muestras a numero_grupos_W (ej: 10 grupos)
    where_col_max_W_ccle = np.random.choice(K, size=numero_grupos_W, replace=False); ## se escojen 10 columnas que tendran los maximos
    where_col_max_W_tcga = np.random.choice(K, size=numero_grupos_W, replace=False); ## se escojen 10 columnas que tendran los maximos
    
    for j in np.unique(groups_W):
        # for i in range(add_W_ccle.shape[1]):
        add_W_ccle[np.where(groups_W == j)[0], where_col_max_W_ccle[j]] = 10 ## se asignan los maximos
        add_W_tcga[np.where(groups_W == j)[0], where_col_max_W_tcga[j]] = 10 ## se asignan los maximos
    
    W0_ccle = np.random.uniform(0,1,[dim_samples[0],K])+add_W_ccle;
    W0_tcga = np.random.uniform(0,1,[dim_samples[1],K])+add_W_tcga;
    
    np.savetxt(ruta+'/co-mod_simulated_H_W/simulated_W0_ccle.csv', W0_ccle, delimiter=',')
    np.savetxt(ruta+'/co-mod_simulated_H_W/simulated_W0_tcga.csv', W0_tcga, delimiter=',')
    
    ## H_I
    add_H_I = {k: np.zeros([K,v]) for k, v in dim_features.items()}
    groups_H_I = {k: np.random.randint(numero_grupos_H, size=v) for k,v in dim_features.items()}
    where_row_max_H = {k: np.random.choice(K, size=numero_grupos_H, replace=False) for k in dim_features.keys()}; ## se escojen 35 columnas que tendran los maximos
    for k, v in add_H_I.items():
        for j in np.unique(groups_H_I[k]):
            # for i in range(v.shape[0]):
            v[where_row_max_H[k][j], np.where(groups_H_I[k] == j)[0]] = 10
    
    H_record_simulated = defaultdict(lambda: np.ndarray(0));
    # H_record_simulated = {k: np.random.uniform(0,1,[K, v])+add_H_I[k] for k, v in dim_features.items()};
    H_record_simulated = {k: np.random.uniform(0,1,[K, v])+add_H_I[k] for k, v in dim_features.items()};
    
    for k, v in H_record_simulated.items():
        np.savetxt(ruta+"/co-mod_simulated_H_W/simulated_H_"+k+'.csv', v, delimiter=',')
    
    ## noise_simulation
    noise_data_0 = {k: np.random.normal(0,5e-3,[dim_samples[0],v]) for k, v in dim_features.items()}
    noise_data_1 = {k: np.random.normal(0,5e-1,[dim_samples[0],v]) for k, v in dim_features.items()}
    
    for k, v in noise_data_0.items():
        np.savetxt(ruta+"/co-mod_simulated_H_W/noise_data_0_"+k+'.csv', v, delimiter=',')
    
    for k, v in noise_data_1.items():
        np.savetxt(ruta+"/co-mod_simulated_H_W/noise_data_1_"+k+'.csv', v, delimiter=',')
        
### Cargar datos
def explore(X):
    for k,v in X.items():
        print(k)
        print(v.shape)
        print(v[0:3,0:3])
    
def norm_min_max(profile):
    from sklearn.preprocessing import MinMaxScaler
    scaler = MinMaxScaler(copy=False, feature_range=(0,1))
    profile = scaler.fit_transform(profile)
    return profile

def normalize_WH_rowH(W0,H0,value=1):
    """
    Normaliza W y H.

    Args
    -------
    W (array) 
    H (array)
    value (int)

    Returns
    -------
    Matrices W y H normalizadas
    """
    vec_sumN = {k: v.shape[1] for k, v in H0.items()}; H = np.concatenate([v for v in H0.values()],axis=1);
    sh = np.sum(H, axis=1); sh[sh == 0] = value; psh = np.divide(value, sh);
    W = defaultdict(lambda: dict()); W={k: np.dot(v,np.diag(sh)) for k, v in W0.items()}; H=np.dot(np.diag(psh),H);
    H_init = defaultdict(lambda: dict());
    for i, t in enumerate(H0.keys()):
        H_init[t] = H[:,0:vec_sumN[t]]
        H = np.delete(H, obj=slice(vec_sumN[t]), axis=1)
    return W, H_init

def compute_XHt_HHt(X_record_ccle, X_record_tcga, H_record,K):
    """
    Computa XHt y HHt.

    Args
    -------
    X_record (dict): data de omicas orignales.

    H_record (dict): Hi donde i corresponde a cada una de las omicas.

    Returns
    -------
    sumXHt (array) calcula

    .. math::
        (\sum_{I}X_{I}H_{I}^T)_{ij}

    sumHHt (dict de array) calcula

    .. math::
        (\sum_{I}H_{I}H_{I}^T)_{ij}

    XHt_record (dict) calcula para cada omica

    .. math:: 
        X_{I}H_{I}^T

    HHt_record (dict) calcula para cada omica

    .. math::
        H_{I}H_{I}^T

    """
    numN = len(X_record_ccle); M=X_record_ccle[list(X_record_ccle.keys())[0]].shape[0]; N=X_record_tcga[list(X_record_tcga.keys())[0]].shape[0];
    XHt_record_ccle=defaultdict(lambda: np.ndarray(0)); XHt_record_tcga=defaultdict(lambda: np.ndarray(0));
    HHt_record = defaultdict(lambda: np.ndarray(0)); sumHHt = defaultdict(lambda: np.zeros([K,K]));
    sumXHt_ccle = np.zeros([M,K]); sumXHt_tcga = np.zeros([N,K]);
    for i in range(0,numN):
        ### XHt_record para ccle y tcga (try: except:)
        XHt_record_ccle[list(X_record_ccle.keys())[i]] = np.dot(X_record_ccle[list(X_record_ccle.keys())[i]],H_record[list(X_record_ccle.keys())[i]].T);
        try: ## cuando haya perfil de drugs, habra un perfil más y saldra error, por lo que solo suma los que hay para tcga
            XHt_record_tcga[list(X_record_tcga.keys())[i]] = np.dot(X_record_tcga[list(X_record_tcga.keys())[i]],H_record[list(X_record_tcga.keys())[i]].T);
        except:
            pass
        ### HHt_record usando ccle index porque tienen todos los perfiles, contando drug
        HHt_record[list(X_record_ccle.keys())[i]] = np.dot(H_record[list(X_record_ccle.keys())[i]],H_record[list(X_record_ccle.keys())[i]].T);
        ### sumXHt para ccle y tcga (try: except:)
        sumXHt_ccle = sumXHt_ccle + XHt_record_ccle[list(X_record_ccle.keys())[i]]; 
        try:
            sumXHt_tcga = sumXHt_tcga + XHt_record_tcga[list(X_record_tcga.keys())[i]];
        except:
            pass
        ### sumHHt para ccle y tcga (try: except:)
        sumHHt["HHt_ccle"] = sumHHt["HHt_ccle"] + HHt_record[list(X_record_ccle.keys())[i]];
        # try: 
        #     sumHHt["HHt_tcga"] = sumHHt["HHt_tcga"] + HHt_record[list(X_record_ccle.keys())[i]];
        # except:
        #     pass
        if i!=0:
            sumHHt["HHt_tcga"] = sumHHt["HHt_tcga"] + HHt_record[list(X_record_ccle.keys())[i]];
        
    return sumXHt_ccle,sumXHt_tcga,sumHHt,XHt_record_ccle,XHt_record_tcga,HHt_record

def compute_HsumTheta_sumHRt_record(H_record, theta_record, R_record):
    """
    Computa Hsumtheta_record y sumHRt_record para cada omica

    Args
    -------
    H_record (dict)

    theta_record (dict)

    sumHRt_record (dict)

    Returns
    -------
    Hsumtheta_record (dict) calcula

    .. math::
        \sum_{t}H_{I}[\\theta_{I}+(\\theta_{I}^{(t)})^{T}]
    
    sumHRt_record (dict) calculo

    .. math::
        \sum_{I\\neq J}H_{J}R_{IJ}^{T}
    """
    numN = len(H_record); Hsumtheta_record = defaultdict(lambda: np.ndarray(0));
    sumHRt_record = defaultdict(lambda: np.ndarray(0));
    sumtheta_record = compute_sumtheta_record(H_record,theta_record);
    for i in range(0,numN):
        Hsumtheta_record[list(H_record.keys())[i]] = np.dot(H_record[list(H_record.keys())[i]],sumtheta_record[list(H_record.keys())[i]]);
        sumHRt_record[list(H_record.keys())[i]] = compute_sumHRt(H_record,R_record,i);
    return Hsumtheta_record, sumHRt_record

def compute_sumtheta_record(H_record,theta_record):
    """
    Computa sumtheta para cada omica.

    Args
    -------
    H_record (dict)

    theta_record (dict)

    Returns
    -------
    sumtheta_record (dict) calcula para cada omica

    .. math:: 
        \sum_{t}\\theta_{I}+(\\theta_{I}^{(t)})^{T}
    """
    numN = len(H_record); sumtheta_record = defaultdict(lambda: np.ndarray(0));
    for i in range(0,numN):
        N = H_record[list(H_record.keys())[i]].shape[1]; sumtheta = np.zeros([N,N]);
        # print("    ** matriz inicial de Theta")
        # print("    -> shape sumtheta: "+str(sumtheta.shape))
        # print("    -> sum sumtheta: "+str(sum(sum(sumtheta))))
        for value in theta_record[list(H_record.keys())[i]]:
            sumtheta = sumtheta+value.T;
            # print("        ** sumando Theta")
            # print("        -> shape sumtheta: "+str(sumtheta.shape))
            # print("        -> sum sumtheta: "+str(sum(sum(sumtheta))))
        sumtheta_record[list(H_record.keys())[i]] = sumtheta;
    return sumtheta_record

def compute_sumHRt(H_record,R_record,index):
    """
    Computa sumHRt.

    Args
    -------
    H_record (dict)

    R_record (dict)

    index (int): indice de la omica a calcular

    Returns
    -------
    sumHRt (array) calcula

    .. math::
        \sum_{I\\neq J}H_{J}R_{IJ}^{T}
    """
    numN = len(H_record); H = H_record[list(H_record.keys())[index]]; K,N_index = H.shape;
    sumHRt = np.zeros([K,N_index]);
    for i in range(0,numN):
        if (str(i) != str(index)) & (R_record[index,i].shape[0] != 0):
            sumHRt = sumHRt + np.dot(H_record[list(H_record.keys())[i]],R_record[index,i].T);
    return sumHRt

def compute_actualize_H_record(H_record, WtX_record_ccle, WtX_record_tcga, Hsumtheta_record, sumHRt_record, WtW, L1, L2, d1, K, miss_profile):
    """
    Actualiza cada uno de los Hi

    Args
    -------
    H_record (dict)

    WtX_record (dict)

    Hsumtheta_record (dict)

    sumHRt_record (dict)

    L1, L2, r2 (int)

    WtW (array)

    K (int) 

    Returns
    -------
    H_record (dict) calcula

    .. math::
        \\frac{h_{ij}^{I}(W^{T}X_{I}+\\frac{\\lambda_{1}}{2}\sum_{t}H_{I}[\\theta_{I}+(\\theta_{I}^{(t)})^{T}]+\\frac{\\lambda_{2}}{2}\sum_{I\\neq J}H_{J}R_{IJ}^{T})_{ij}}{((W^{T}W+\\gamma_{2}e_{K \\times K})H_{I})_{ij}}
    """
    numN = len(H_record); 
    for i in range(0,numN):
        if miss_profile[0] in list(H_record.keys())[i] and miss_profile[0] != "None":
            numerator = H_record[list(H_record.keys())[i]] * (WtX_record_ccle[list(H_record.keys())[i]] + L1/2 * Hsumtheta_record[list(H_record.keys())[i]] + L2/2 * sumHRt_record[list(H_record.keys())[i]]);
            denominator = np.dot((WtW['W0_ccle'] + d1 * np.ones([K,K])), H_record[list(H_record.keys())[i]]) + np.finfo(float).eps;
        else:
            numerator = H_record[list(H_record.keys())[i]] * (WtX_record_ccle[list(H_record.keys())[i]] + WtX_record_tcga[list(H_record.keys())[i]] + L1/2 * Hsumtheta_record[list(H_record.keys())[i]] + L2/2 * sumHRt_record[list(H_record.keys())[i]]);
            denominator = np.dot((WtW['W0_ccle'] + WtW['W0_tcga'] + d1 * np.ones([K,K])), H_record[list(H_record.keys())[i]]) + np.finfo(float).eps;
        H_record[list(H_record.keys())[i]] =  numerator / denominator;
    return H_record

def compute_actualize_H_record_kernel(H_record, WtX_record_ccle, WtX_record_tcga, WtW, d1, K, miss_profile):
    """
    Actualiza cada uno de los Hi

    Args
    -------
    H_record (dict)

    WtX_record (dict)

    r2 (int)

    WtW (array)

    K (int) 

    Returns
    -------
    H_record (dict) calcula

    .. math::
        \\frac{h_{ij}^{I}(W^{T}X_{I})_{ij}}{((W^{T}W+\\gamma_{2}e_{K \\times K})H_{I})_{ij}}
    """
    numN = len(H_record); 
    for i in range(0,numN):
        if miss_profile[0] in list(H_record.keys())[i]:
            numerator = H_record[list(H_record.keys())[i]] * (WtX_record_ccle[list(H_record.keys())[i]]);
            denominator = np.dot((WtW['W0_ccle'] + d1 * np.ones([K,K])), H_record[list(H_record.keys())[i]]) + np.finfo(float).eps;
        else:
            numerator = H_record[list(H_record.keys())[i]] * (WtX_record_ccle[list(H_record.keys())[i]] + WtX_record_tcga[list(H_record.keys())[i]]);
            denominator = np.dot((WtW['W0_ccle'] + WtW['W0_tcga'] + d1 * np.ones([K,K])), H_record[list(H_record.keys())[i]]) + np.finfo(float).eps;
        H_record[list(H_record.keys())[i]] =  numerator / denominator;
    return H_record

def compute_diff(WtW,H_record,HHt_record,Hsumtheta_record,sumHRt_record,L1,L2,r1,r2,d1,K):
    numN = len(H_record);
    D2 = 0;
    for i in range(0,numN):
        Hsumtheta = Hsumtheta_record[list(H_record.keys())[i]];
        HtheHt = np.dot(Hsumtheta, H_record[list(H_record.keys())[i]].T);
        D2 = D2 + np.trace(HtheHt);
    diff2 = -L1/2*D2;
    D3 = 0;
    for i in range(0,numN):
        sumHRt = sumHRt_record[list(H_record.keys())[i]];
        HRHt = np.dot(H_record[list(H_record.keys())[i]],sumHRt.T);
        D3 = D3 + np.trace(HRHt);
    # diff3 = -L2/2*D3;
    diff3 = -L2*D3;
    diff4_ccle = r1*np.trace(WtW['W0_ccle']);
    diff4_tcga = r2*np.trace(WtW['W0_tcga']);
    D5 = 0;
    for i in range(0,numN):
        HHt = HHt_record[list(H_record.keys())[i]].T;
        D5 = D5 + np.dot(np.dot(np.ones([1,K]),HHt),np.ones([K,1]));
    diff5 = d1*D5[0][0];
    return diff2,diff3,diff4_ccle,diff4_tcga,diff5
 
def StopCriterion_rule1(VtV_ccle,VtV_tcga,WtW,W,sumXHt_ccle,sumHHt,sumXHt_tcga,diff2,diff3,diff4_ccle,diff4_tcga,diff5):
    WtXHt_ccle = np.dot(W['W0_ccle'].T,sumXHt_ccle);
    WtWHHt_ccle = np.dot(WtW['W0_ccle'],sumHHt["HHt_ccle"]);
    diff1_ccle = np.trace(VtV_ccle) - 2*np.trace(WtXHt_ccle) + np.trace(WtWHHt_ccle);
    WtXHt_tcga = np.dot(W['W0_tcga'].T,sumXHt_tcga);
    WtWHHt_tcga = np.dot(WtW['W0_tcga'],sumHHt["HHt_tcga"]);
    diff1_tcga = np.trace(VtV_tcga) - 2*np.trace(WtXHt_tcga) + np.trace(WtWHHt_tcga);
    retVal = diff1_ccle+diff1_tcga+diff2+diff3+diff4_ccle+diff4_tcga+diff5;
    return retVal,diff1_ccle, diff1_tcga

def jNMF_module(H,t,path,featureLabel, print_savePath, sample, useMalaCard, method_clustering):
    from itertools import product
    from collections import defaultdict 
    comodule = defaultdict(lambda: defaultdict(dict));
    comodule_count = defaultdict(lambda: defaultdict(int));
    connectivity_matrix = defaultdict(lambda: defaultdict(0));
    
    if useMalaCard:
        for perfil in H.keys():
            coord_matrix = list(range(0,H[perfil].shape[1]))
            # coord_matrix = product(coord_matrix_k, coord_matrix_mol)
            coord_matrix = product(coord_matrix, coord_matrix)
            connectivity_matrix[perfil] = {k: 0 for k in coord_matrix}
    else:
        for perfil in H.keys():
            if "mrna" in perfil or "cnv" in perfil or "meth" in perfil or "mut" in perfil: # 
                ## complex calculation for these profiles
                pass
            else:
                coord_matrix = list(range(0,H[perfil].shape[1]))
                coord_matrix = product(coord_matrix, coord_matrix)
                connectivity_matrix[perfil] = {k: 0 for k in coord_matrix}
                
    if method_clustering == "weight": 
        ##quantiles para cada H
        H_q3 = {k: np.quantile(v, .75, axis=1) for k, v in H.items()};
        
        if sample != "sample":
            for perfil, value in H.items():
                if print_savePath == True:
                    try:
                        # Create target Directory
                        os.mkdir(path+'/'+perfil+'_cluster_weight')
                        print("Directory " , path+'/'+perfil+'_cluster_weight' ,  " Created ")
                    except FileExistsError:
                        print("Directory " , path+'/'+perfil+'_cluster_weight' ,  " already exists")
                
                # limpiar carpeta para evitar incluir resultados de experimentos anteriores
                folder=path+"/"+perfil+'_cluster_weight'
                for filename in os.listdir(folder):
                    file_path = os.path.join(folder, filename)
                    os.unlink(file_path)
                    
                for i in range(0, value.shape[0]):
                    comodule[perfil].update({'co-md_'+str(i): list(np.where(value[i,:] > H_q3[perfil][i] ))});
                    comodule_count[perfil]['co-md_'+str(i)] = len(comodule[perfil]['co-md_'+str(i)][0])
                    
                    ## Connectivity matrix
                    if useMalaCard:
                        matriz = product(list(comodule[perfil]['co-md_'+str(i)][0]), list(comodule[perfil]['co-md_'+str(i)][0]));
                        matriz = list(matriz);
                        for j in range(0,len(matriz)):
                            connectivity_matrix[perfil][matriz[j]] = 1
                    else:
                        if "mrna" in perfil or "cnv" in perfil or "meth" in perfil or "mut" in perfil: #
                            pass
                        else:
                            matriz = product(list(comodule[perfil]['co-md_'+str(i)][0]), list(comodule[perfil]['co-md_'+str(i)][0]));
                            matriz = list(matriz);
                            for j in range(0,len(matriz)):
                                connectivity_matrix[perfil][matriz[j]] = 1
                    if print_savePath == True:
                        listaFeatures = [featureLabel[perfil][v] for v in comodule[perfil]['co-md_'+str(i)][0]]
                        with open(path+'/'+perfil+'_cluster_weight'+'/'+perfil+'_co-md_'+str(i)+'.txt', 'w') as f:
                            f.writelines("%s\n" % l for l in listaFeatures)
    elif method_clustering == "H_first_second_max":
        ## maximos_first
        H_maximos = {perfil: np.argmax(v, axis=0) for perfil, v in H.items()};
        
        H_maximos_valor = {perfil: np.max(v, axis=0) for perfil, v in H.items()}
        
        # Outliers
        unicos = {perfil: np.unique(H_maximos[perfil]) for perfil in H_maximos.keys()}
        
        def unicos_H_max_Q2(unicos, H, H_maximos, H_maximos_valor, perfil):
            for i in unicos[perfil]:
                partial_H = H[perfil][i,np.where(H_maximos[perfil] == i)[0]]
                Q1 = np.quantile(partial_H, .25)
                Q2 = np.quantile(partial_H, .50)
                if "mrna" in perfil:
                    H_maximos[perfil][np.where((H_maximos_valor[perfil] < Q2) & (H_maximos[perfil] == i))] = 9999
                else:
                    H_maximos[perfil][np.where((H_maximos_valor[perfil] < Q1) & (H_maximos[perfil] == i))] = 9999
                
            return H_maximos[perfil]
        
        for perfil in H_maximos.keys():
            H_maximos[perfil] = unicos_H_max_Q2(unicos, H, H_maximos, H_maximos_valor, perfil)
    
        ## maximos_second
        H_maximos_second = {perfil: np.argsort(-v, axis=0)[1] for perfil, v in H.items()}
        
        H_maximos_valor = {perfil: np.abs(np.sort(-v, axis=0)[1]) for perfil, v in H.items()}
        
        # Outliers
        unicos = {perfil: np.unique(H_maximos_second[perfil]) for perfil in H_maximos_second.keys()}
        
        def unicos_H_max_second_Q2(unicos, H, H_maximos_second, H_maximos_valor, perfil):
            for i in unicos[perfil]:
                partial_H = H[perfil][i,np.where(H_maximos_second[perfil] == i)[0]]
                Q2 = np.quantile(partial_H, .50)
                
                # los valores maximos proximos a cero se omiten por lo que se fija su valor en 9999
                # al ser llamados mas abajo en la formacion de co-modulos, no se encontraran y no 
                # entraran al comodulo.
                H_maximos_second[perfil][np.where((H_maximos_valor[perfil] < Q2) & (H_maximos_second[perfil] == i))] = 9999
            
            return H_maximos_second[perfil]
        
        for perfil in H_maximos_second.keys():
            H_maximos_second[perfil] = unicos_H_max_second_Q2(unicos, H, H_maximos_second, H_maximos_valor, perfil)
        if sample != "sample":
            # print("")
            # print('----------------------------------------------------------')
            # print("* Saving clusters in: ")
            for perfil, value in H.items():
                if print_savePath == True:
                    try:
                        # Create target Directory
                        os.mkdir(path+'/'+perfil)
                        # print("Directory " , path+'/'+perfil ,  " Created ")
                    except FileExistsError:
                        pass
                        # print("Directory " , path+'/'+perfil ,  " already exists")
                
                # limpiar carpeta para evitar incluir resultados de experimentos anteriores
                folder=path+'/'+perfil
                for filename in os.listdir(folder):
                    file_path = os.path.join(folder, filename)
                    os.unlink(file_path)
                
                for i in range(0, value.shape[0]):
                    comodule[perfil].update({'co-md_'+str(i): list(np.where(i == H_maximos[perfil]))});
                    adicional_segundo = np.where(i == H_maximos_second[perfil]);
                    comodule[perfil]['co-md_'+str(i)][0] = np.append(comodule[perfil]['co-md_'+str(i)][0], adicional_segundo[0]);
                    comodule_count[perfil]['co-md_'+str(i)] = len(comodule[perfil]['co-md_'+str(i)][0])
                    
                    ## Connectivity matrix
                    if useMalaCard:
                        matriz = product(list(comodule[perfil]['co-md_'+str(i)][0]), list(comodule[perfil]['co-md_'+str(i)][0]));
                        matriz = list(matriz);
                        for j in range(0,len(matriz)):
                            connectivity_matrix[perfil][matriz[j]] = 1
                    else:
                        if "mrna" in perfil or "cnv" in perfil or "meth" in perfil or "mut" in perfil: #
                            pass
                        else:
                            matriz = product(list(comodule[perfil]['co-md_'+str(i)][0]), list(comodule[perfil]['co-md_'+str(i)][0]));
                            matriz = list(matriz);
                            for j in range(0,len(matriz)):
                                connectivity_matrix[perfil][matriz[j]] = 1
                    if print_savePath == True:
                        listaFeatures = [featureLabel[perfil][v] for v in comodule[perfil]['co-md_'+str(i)][0]]
                        with open(path+'/'+perfil+'/'+perfil+'_co-md_'+str(i)+'.txt', 'w') as f:
                            f.writelines("%s\n" % l for l in listaFeatures)
    return comodule, comodule_count, connectivity_matrix

def corr2_coeff(A, B):
    # Rowwise mean of input arrays & subtract from input arrays themeselves
    A_mA = A - A.mean(1)[:, None]
    B_mB = B - B.mean(1)[:, None]

    # Sum of squares across rows
    ssA = (A_mA**2).sum(1)
    ssB = (B_mB**2).sum(1)

    # Finally get corr coeff
    return np.dot(A_mA, B_mB.T) / (np.sqrt(np.dot(ssA[:, None],ssB[None])) + np.finfo(float).eps)

def jNMF_module_patients_cellLines(best_W, infoFeatures_tcga, infoFeatures_ccle, param, path, sample):
    module_patient_cell = defaultdict(list)
    ccle_dict = defaultdict(int)
    ccle_dict = {k: 0 for k in infoFeatures_ccle}
    tcga_dict = defaultdict(int)
    tcga_dict = {k: 0 for k in infoFeatures_tcga}
    for i in range(0,best_W["W0_ccle"].shape[1]):
        Q3 = np.quantile(best_W["W0_ccle"][:,i], .75)
        ccle_ = list(np.array(infoFeatures_ccle)[np.where(best_W["W0_ccle"][:,i] > Q3),][0])
        for v in ccle_:
            ccle_dict[v] += 1
        
        Q3 = np.quantile(best_W["W0_tcga"][:,i], .75)
        tcga_ = list(np.array(infoFeatures_tcga)[np.where(best_W["W0_tcga"][:,i] > Q3),][0])
        for v in tcga_:
            tcga_dict[v] += 1
        
        module_patient_cell['co-md_'+str(i)] = ccle_ + tcga_
        
    ccle_ = pd.DataFrame.from_dict(ccle_dict, orient='index').sort_values(by=[0], ascending=False).T
    tcga_ = pd.DataFrame.from_dict(tcga_dict, orient='index').sort_values(by=[0], ascending=False).iloc[0:best_W["W0_ccle"].shape[0]].T
    tcga_.columns = list(map(lambda x: x.replace("TCGA-", ""), tcga_.columns))
    
    if sample != "sample":        
        porc_modulos_no_vacios_ccle_=np.sum(ccle_.values != 0) / best_W["W0_ccle"].shape[0];
    else:
        porc_modulos_no_vacios_ccle_=0
    return module_patient_cell, porc_modulos_no_vacios_ccle_

def check_symmetric(a, rtol=1e-05, atol=1e-08):
    return np.allclose(a, a.T, rtol=rtol, atol=atol)


def silueta(K, sample_silhouette_values, cluster_labels, ax):
    # import matplotlib.pyplot as plt
    import matplotlib.cm as cm
    y_lower = 10
    for i in range(K):
        # Aggregate the silhouette scores for samples belonging to
        # cluster i, and sort them
        ith_cluster_silhouette_values = \
            sample_silhouette_values[cluster_labels == i]

        ith_cluster_silhouette_values.sort()

        size_cluster_i = ith_cluster_silhouette_values.shape[0]
        y_upper = y_lower + size_cluster_i

        color = cm.nipy_spectral(float(i) / K)
        ax.fill_betweenx(np.arange(y_lower, y_upper),
                          0, ith_cluster_silhouette_values,
                          facecolor=color, edgecolor=color, alpha=0.7)

        # Compute the new y_lower for next plot
        y_lower = y_upper + 10  # 10 for the 0 samples
        
    return ax

def func_ccle_groups(best_W, mrna_rows_ccle, pac_por_comod_ccle, path, projects):
    rec_surv = pd.DataFrame(data=np.zeros([best_W["W0_ccle"].shape[0], best_W["W0_ccle"].shape[1]]), index=list(map(lambda x: x, mrna_rows_ccle)))
    for i in range(0,best_W["W0_ccle"].shape[0]):
        rec_surv.iloc[i,np.argsort(-best_W["W0_ccle"][i,:], axis=0)[0]] = 1 ## Selecciona la fila de paciente y busca donde es el maximo
        
    def find_nearest(array, value):
        array = np.asarray(array)
        idx = (np.abs(array - value)).argmin()
        return idx
    
    cuantos_co_md = pac_por_comod_ccle ## numero de pacientes permitidos en cada cluster, orignalmente 30
    W_max = np.max(best_W["W0_ccle"], axis=0) # los maximos por K clusters
    W_max[np.where(np.sum(rec_surv)<cuantos_co_md)[0]] = -9999 ## cuales de ellos son menores a cuantos_co_md
    b = mrna_rows_ccle
    menores_Xpac = np.where(np.sum(rec_surv)<cuantos_co_md)[0]
    for i in range(0,menores_Xpac.shape[0]): ## recorrer todos los co-md que tienen menos de X pacientes
        a = rec_surv[menores_Xpac[i]][np.where(rec_surv[menores_Xpac[i]]>0)[0]].index
        if len(a) > 0:
            for j in range(0,len(a)):
                d = np.where(rec_surv.loc[a[j]]>0)[0][0] ## buscar posicion actual de a[j]
                rec_surv.loc[a[j], d] = 0 ## quitar esa posicion
                c = np.max(best_W["W0_ccle"][b.index(a[j]),]) ## maximo en best_W["W0_tcga"] para esa fila
                # print(W_max[find_nearest(W_max, c)])
                rec_surv.loc[a[j],find_nearest(W_max, c)] = 1 ## nueva posicion en co-md
    np.where(np.sum(rec_surv,axis=0)>0)
    
    rec_surv = pd.melt(rec_surv.reset_index(), id_vars="index")
    rec_surv  = rec_surv.loc[rec_surv.value != 0,:]
    rec_surv.rename(columns={'variable': 'cluster'}, inplace=True)
    rec_surv.drop(["value"], axis=1, inplace=True)
    rec_surv.to_csv(path+'/co-mod_observations_clusters/observations_clusters_for_project_'+projects[0]+'_using_best_W.csv')
    
def hp_metrics_plots(sample, path, central_path, command, merged):
    # ---------------------------------------------------------------------------------------- #
    # ### ........... Biological data selection of hyperparameters: metric plots ........... ####
    # ---------------------------------------------------------------------------------------- #
    if sample != "sample":
        print("")
        print('----------------------------------------------------------')
        print("* Generating Bio data metric plots: enrich_over_K, geneRatio_avg, puntosTotales, No.CurvasSurivial_p, porc_Cluster_CCLE, FuncObj")
        path2script = '--vanilla ' + path + "/25_result_selectionHyperParams.R"
        cmd = [command, path2script] + ['empty'+"__"+merged.path] #cmd = [command, path2script] + args. Ver pagina rPubs.K-15-r1_10-r2_10-L1_10-L2_10-d1_10
        x = subprocess.Popen(cmd).wait()
        if x == 0:
            print("Ok, metric plots is saved in co-mod_suplementarios!")
        else:
            print("An error ocurred in 25_result_selectionHyperParams.R script")
        print('---------------------------------------------------------- \n')
        
    # ---------------------------------------------------------------------------------------- #
    # ### ........... Synthetic data selection of hyperparameters: metric plots ........... ####
    # ---------------------------------------------------------------------------------------- #
    if sample == "sample":
        print("")
        print('----------------------------------------------------------')
        print("* Generating Synthetic data metric plots: rss, r2, No. asoc. detected / No. asoc. real, perc_true_identified")
        path2script = '--vanilla ' + path + "/26_result_SimulData_r2_adjusted.R"
        cmd = [command, path2script] + ['empty'+"__"+path] #cmd = [command, path2script] + args. Ver pagina rPubs.K-15-r1_10-r2_10-L1_10-L2_10-d1_10
        x = subprocess.Popen(cmd).wait()
        if x == 0:
            print("Ok, metric plots is saved in co-mod_suplementarios!")
        else:
            print("An error ocurred in 26_result_SimulData_r2_adjusted.R script")
        print('---------------------------------------------------------- \n')
        
    # ---------------------------------------------------------------------------------------- #
    # ### ........... Synthetic data selection of hyperparameters: metric plots ........... ####
    # ---------------------------------------------------------------------------------------- #
    if sample == "sample":
        print("")
        print('----------------------------------------------------------')
        print("* Generating Synthetic data metric plots: average metrics")
        path2script = '--vanilla ' + path + "/28_result_SimulData_r2_adjusted_secondTry.R"
        cmd = [command, path2script] + ['empty'+"__"+path] #cmd = [command, path2script] + args. Ver pagina rPubs.K-15-r1_10-r2_10-L1_10-L2_10-d1_10
        x = subprocess.Popen(cmd).wait()
        if x == 0:
            print("Ok, metric plots is saved in co-mod_suplementarios!")
        else:
            print("An error ocurred in 28_result_SimulData_r2_adjusted_secondTry.R script")
        print('---------------------------------------------------------- \n')
        
    # ---------------------------------------------------------------------------------------- #
    # ### ........... Synthetic data selection of hyperparameters: impact hyperparams plots ........... ####
    # ---------------------------------------------------------------------------------------- #
    if sample == "sample":
        print("")
        print('----------------------------------------------------------')
        print("* Generating Synthetic data metric plots: average metrics")
        path2script = '--vanilla ' + path + "/29_result_SimulData_ImpactHyperParams.R"
        cmd = [command, path2script] + ['empty'+"__"+path] #cmd = [command, path2script] + args. Ver pagina rPubs.K-15-r1_10-r2_10-L1_10-L2_10-d1_10
        x = subprocess.Popen(cmd).wait()
        if x == 0:
            print("Ok, hyperparams impact plot is saved in co-mod_suplementarios!")
        else:
            print("An error ocurred in 29_result_SimulData_ImpactHyperParams.R script")
        print('---------------------------------------------------------- \n')  
    # np.where(np.sum(rec_surv,axis=0)>0)


def LoadSyntheticData(ruta, dim_features):
    ### Cargar datos
    try:
        W_simulated = dict()
        W_simulated["W0_ccle"] = np.loadtxt(ruta+'/co-mod_simulated_H_W/simulated_W0_ccle.csv', delimiter=',')#+(noise_proportion*np.random.normal(0,1,[dim_samples[0],K]))
        W_simulated["W0_tcga"] = np.loadtxt(ruta+'/co-mod_simulated_H_W/simulated_W0_tcga.csv', delimiter=',')#+(noise_proportion*np.random.normal(0,1,[dim_samples[1],K]))
        
        H_record_simulated = dict()
        H_record_simulated = {k: np.loadtxt(ruta+'/co-mod_simulated_H_W/simulated_H_'+k+'.csv', delimiter=',') for k in dim_features.keys()}

        noise_data_0 = dict()
        noise_data_0 = {k: np.loadtxt(ruta+'/co-mod_simulated_H_W/noise_data_0_'+k+'.csv', delimiter=',') for k in dim_features.keys()}
        noise_data_1 = dict()
        noise_data_1 = {k: np.loadtxt(ruta+'/co-mod_simulated_H_W/noise_data_1_'+k+'.csv', delimiter=',') for k in dim_features.keys()}
        
        # W - asociaciones pacientes
        max_W_simulated = {k: np.argmax(v, axis=1) for k,v in W_simulated.items()}
        max_columna_W = {k: pd.DataFrame([range(len(v)),v], index=['indice','grupo']).T for k,v in max_W_simulated.items()};
        max_columna_W_ = dict()
        for k, v in max_columna_W.items():
            W_asociaciones_por_paciente=[]
            for j in np.unique(v.grupo):
                W_asociaciones_por_paciente = W_asociaciones_por_paciente  + list(combinations(v.indice[v.grupo == j],2))
            max_columna_W_[k] = W_asociaciones_por_paciente
            
        # H - asociaciones moleculas
        max_H_record_simulated = {k: np.argmax(v, axis=0) for k,v in H_record_simulated.items()}
        max_columna_H = {k: pd.DataFrame([range(len(v)),v], index=['indice','grupo']).T for k,v in max_H_record_simulated.items()};
        max_columna_H_ = dict()
        for k, v in max_columna_H.items():
            H_asociaciones_por_moleculas=[]
            for j in np.unique(v.grupo):
                H_asociaciones_por_moleculas = H_asociaciones_por_moleculas  + list(combinations(v.indice[v.grupo == j],2))
            max_columna_H_[k] = H_asociaciones_por_moleculas
            
        ## Theta constraints using H_I
        mayor_de_10 = {k: np.argmax(v>=10, axis=0) for k,v in H_record_simulated.items()}
        theta_permutaciones = dict()
        for k, v in mayor_de_10.items():
            lista_permutaciones =[]
            for j in range(v.shape[0]):
                lista_permutaciones = lista_permutaciones  + list(permutations(np.where(v==j)[0],2))
            theta_permutaciones[k] = lista_permutaciones
            
        # matriz theta
        theta_record = {k: np.zeros([v.shape[1], v.shape[1]]) for k,v in H_record_simulated.items()}
        for k,v in theta_record.items():
            for i in theta_permutaciones[k]:
                v[i] = 1;
        
        ## R constraints using H_I
        combinacion_perfiles = list(combinations(list(H_record_simulated.keys()), 2));
        comb_perfiles_dict = dict()
        for i in combinacion_perfiles:
            comb_perfiles_dict[i] = np.zeros([H_record_simulated[i[0]].shape[1], H_record_simulated[i[1]].shape[1]])
        compilado_dict=dict()
        for k,v in comb_perfiles_dict.items():
            a = mayor_de_10[k[0]]
            b = mayor_de_10[k[1]]
            c_range = np.intersect1d(a,b)
            d_list = []
            for i in c_range:
                d_list = d_list + list(product(list(np.where(mayor_de_10[k[0]]==i)[0]), list(np.where(mayor_de_10[k[1]]==i)[0])))
            compilado_dict[k] = d_list
            
        matrizFinal = []
        for k in H_record_simulated.keys():
            otraMatriz = []
            for k_ in H_record_simulated.keys():
                if (k,k_) in compilado_dict.keys():
                    for j in compilado_dict[(k,k_)]:
                        comb_perfiles_dict[(k,k_)][j] = 1
                    matriz = comb_perfiles_dict[(k,k_)].T
                elif k==k_:
                    matriz = np.zeros([H_record_simulated[k].shape[1], H_record_simulated[k_].shape[1]])
                else:
                    for j in compilado_dict[(k_,k)]:
                        comb_perfiles_dict[(k_,k)][j] = 1
                    matriz = comb_perfiles_dict[(k_,k)]
                otraMatriz.append(matriz);
            matrizFinal.append(np.array(otraMatriz, dtype=object)) ## ir guardando los arrays en un lista
            R_record = np.stack(matrizFinal, axis=-1)
            
        SynData = {'W_simulated': W_simulated, 'H_record_simulated': H_record_simulated, 'theta_record': theta_record, 
                   'R_record': R_record, 'max_columna_H_': max_columna_H_, 'max_columna_W_': max_columna_W_, 
                   'noise_data_0': noise_data_0, 'noise_data_1': noise_data_1};
        return SynData
        
    except IOError:
        print("You must first create simulated_W0_ccle, simulated_W0_tcga and the simulated_H_I")
